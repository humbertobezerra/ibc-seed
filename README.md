# Introduction
[![Build Status](https://travis-ci.org/rtang03/ibc-seed.svg?branch=master)](https://travis-ci.org/rtang03/ibc-seed) [![Hiptest Status](https://hiptest.net/badges/test_run/26342)](https://hiptest.net#/projects/22146/testRuns/26342)

A seed project for Hyperledger application, written with Angular2/Express (Typescript). 

The seed provides the following features:
- Ready to go, statically typed build system using gulp for working with TypeScript.
- Production and development builds.
- Sample unit tests with Jasmine and Karma including code coverage via istanbul.
- End-to-end tests with Protractor.
- Manager of your type definitions using typings, and manual typings
- Deployable to Bluemix
- (intentionally no livereload)

Todo:
- autoprefixer, css-lint
- report to Hiptest

# How to start
- build dev; run dev

`npm start`

- build dev only

`npm run build.dev`

- build dev; excute frontend/backend tests

`npm run build.run.test`

- run dev (without build)

`npm run serve.dev`

- build dev; execute frontend tests, for Travis CI purpose

`npm test`

- run prod (without build)

`npm run serve.prod ==> need fixing`

# Configuration
Default application configuration

`host = 127.0.0.1;`

`port = 5556;`


