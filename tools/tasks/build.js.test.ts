/**
 * Created by tangross on 12/5/2016.
 */
import { join} from "path";
import { APP_DEST, APP_SRC /*, BOOTSTRAP_MODULE */ , TOOLS_DIR } from "../config";
import { tsProjectFn, templateLocals } from "../utils";

export = (gulp, plugins) => {
  let tsProject = tsProjectFn(plugins);

  return function () {
    let src = [
      "typings/browser.d.ts",
      TOOLS_DIR + "/manual_typings/**/*.d.ts",
      join(APP_SRC, "**/*.ts"),
      "!" + join(APP_SRC, "**/*.e2e-spec.ts")
      // "!" + join(APP_SRC, `${BOOTSTRAP_MODULE}.ts`)  // don't what is its use
    ];
    let result = gulp.src(src)
      .pipe(plugins.plumber())
      .pipe(plugins.sourcemaps.init())
      .pipe(plugins.inlineNg2Template({
        base: APP_SRC,
        useRelativePaths: false
      }))
      .pipe(plugins.typescript(tsProject));

    return result.js
      .pipe(plugins.sourcemaps.write())
      .pipe(plugins.template(templateLocals())) // added by RT3
      .pipe(gulp.dest(APP_DEST));
  };
};
