/**
 * Created by tangross on 13/5/2016.
 */
import {join} from "path";
import {SERVER_DEST} from "../config";
let jasmineNode = require("gulp-jasmine-node");

export = function testServer(gulp, plugins) {
  return function () {
    let src = [
      join(SERVER_DEST, "spec/**/*.spec.js"),
    ];
    return gulp.src(src)
      .pipe(jasmineNode({
        timeout: 10000,
        color: true
      }));
  };
};
