import {argv} from "yargs";
import * as chalk from "chalk";
import { join } from "path";

const ENVIRONMENTS = {
  DEVELOPMENT: "dev",
  PRODUCTION: "prod"
};

const app_base = "base";
const port     = "port";
export const APP_BASE             = argv[app_base] || "/";
export const PORT                 = argv[port] || 3000;
export const APP_SRC              = "client_src";
export const TOOLS_DIR            = "tools";
export const SERVER_SRC           = "server_src";
export const APP_TITLE            = "Dashboard-Seed";
export const ASSETS_SRC           = `${APP_SRC}/assets`;
export const ENV                  = getEnvironment();
export const APP_DEST             = `dist/${ENV}`;
export const JS_DEST              = `${APP_DEST}/js`;
export const CSS_DEST             = `${APP_DEST}/css`;
export const FONT_DEST            = `${APP_DEST}/fonts`;
export const ASSETS_DEST          = `${APP_DEST}/assets`;
export const SERVER_DEST          = `dist/server`;
export const TMP_DIR              = "tmp";
export const CSS_PROD_BUNDLE      = "all.css";
export const JS_PROD_SHIMS_BUNDLE = "shims.js";
export const JS_PROD_APP_BUNDLE   = "app.js";
export const ENABLE_HOT_LOADING   = !!argv["hot-loader"];
export const BOOTSTRAP_MODULE     = ENABLE_HOT_LOADING ? "hot_loader_main" : "main";
export const PROJECT_ROOT         = join(__dirname, "../..");


export const FONTS_DEST = `${APP_DEST}/fonts`;
export const FONTS_SRC1 = "node_modules/bootstrap/dist/fonts";
export const FONTS_SRC2 = "node_modules/font-awesome/fonts";

if (ENABLE_HOT_LOADING) {
  console.log(chalk.bgRed.white.bold("The hot loader is temporary disabled."));
  process.exit(0);
}

interface InjectableDependency {
  src: string;
  inject: string | boolean;
  dest?: string;
}

// declare NPM dependencies (Note that globs should not be injected).
export const DEV_NPM_DEPENDENCIES: InjectableDependency[] = normalizeDependencies([
  { src: "systemjs/dist/system-polyfills.src.js", inject: "shims", dest: JS_DEST },
  { src: "zone.js/dist/zone.js", inject: "libs", dest: JS_DEST },
  { src: "reflect-metadata/Reflect.js", inject: "shims", dest: JS_DEST },
  { src: "es6-shim/es6-shim.js", inject: "shims", dest: JS_DEST },
  { src: "systemjs/dist/system.src.js", inject: "shims", dest: JS_DEST },
  { src: "rxjs/bundles/Rx.js", inject: "libs", dest: JS_DEST }
]);

export const PROD_NPM_DEPENDENCIES: InjectableDependency[] = normalizeDependencies([
  { src: "systemjs/dist/system-polyfills.src.js", inject: "shims" },
  { src: "reflect-metadata/Reflect.js", inject: "shims" },
  { src: "es6-shim/es6-shim.min.js", inject: "shims" },
  { src: "systemjs/dist/system.js", inject: "shims" },
  { src: "angular2/bundles/angular2-polyfills.min.js", inject: "libs" },
  { src: "rxjs/bundles/Rx.js", inject: "libs", dest: JS_DEST },
  { src: "angular2/bundles/angular2.js", inject: "libs", dest: JS_DEST },
  { src: "angular2/bundles/router.js", inject: "libs", dest: JS_DEST },
  { src: "angular2/bundles/http.js", inject: "libs", dest: JS_DEST },
  { src: "jquery/dist/jquery.min.js", inject: "libs", dest: JS_DEST },
]);

// declare local files that needs to be injected
export const APP_ASSETS: InjectableDependency[] = [
  { src: `${ASSETS_SRC}/main.css`, inject: true, dest: CSS_DEST },
  { src: `${ASSETS_SRC}/bootstrap/bootstrap.css`, inject: true, dest: CSS_DEST },
  { src: `${ASSETS_SRC}/font-awesome-4.5.0/css/font-awesome.min.css`, inject: true, dest: CSS_DEST }
];

export const DEV_DEPENDENCIES = DEV_NPM_DEPENDENCIES.concat(APP_ASSETS);
export const PROD_DEPENDENCIES = PROD_NPM_DEPENDENCIES.concat(APP_ASSETS);

// systemsJS Configuration.
const SYSTEM_CONFIG_DEV: any = {
  defaultJSExtensions: true,
  packageConfigPaths: [
    `${this.APP_BASE}node_modules/*/package.json`,
    `${this.APP_BASE}node_modules/**/package.json`,
    `${this.APP_BASE}node_modules/@angular/*/package.json`
  ],
  paths: {
    [this.BOOTSTRAP_MODULE]: `${this.APP_BASE}${this.BOOTSTRAP_MODULE}`,
    // "rxjs/*": `node_modules/rxjs/*`,
    "rxjs/*": `${this.APP_BASE}rxjs/*`,
    "app/*": `/app/*`,
    "*": `${this.APP_BASE}node_modules/*`
  },
  packages: {
    rxjs: { defaultExtension: false }
  }
};

export const SYSTEM_CONFIG = SYSTEM_CONFIG_DEV;

export const SYSTEM_BUILDER_CONFIG: any = {
  defaultJSExtensions: true,
  packageConfigPaths: [
    join(this.PROJECT_ROOT, "node_modules", "*", "package.json"),
    join(this.PROJECT_ROOT, "node_modules", "@angular", "*", "package.json")
  ],
  paths: {
    [`${this.TMP_DIR}/*`]: `${this.TMP_DIR}/*`,
    "*": "node_modules/*"
  },
  packages: {
    "@angular/core": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/compiler": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/common": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/http": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/platform-browser": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/platform-browser-dynamic": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/router-deprecated": {
      main: "index.js",
      defaultExtension: "js"
    },
    "@angular/router": {
      main: "index.js",
      defaultExtension: "js"
    },
    "rxjs": {
      defaultExtension: "js"
    }
  }
};

function getEnvironment() {
  let env = "_";
  let base: string[] = argv[env];
  env = "env";
  let prodKeyword = !!base.filter(o => o.indexOf(ENVIRONMENTS.PRODUCTION) >= 0).pop();
  if (base && prodKeyword || argv[env] === ENVIRONMENTS.PRODUCTION) {
    return ENVIRONMENTS.PRODUCTION;
  } else {
    return ENVIRONMENTS.DEVELOPMENT;
  }
}

function normalizeDependencies(deps: InjectableDependency[]) {
  deps
    .filter((d: InjectableDependency) => !/\*/.test(d.src)) // skip globs
    .forEach((d: InjectableDependency) => d.src = require.resolve(d.src));
  return deps;
}
