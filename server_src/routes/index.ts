/// <reference path="../../node_modules/rxjs/Rx.d.ts" />

import * as express               from "express";
import { Observable }             from "rxjs/Observable";

import { chaincode, REST_CALLS }  from "../hyperledger.cc.rest";
import { caConnector }            from "../ca.connector";

const TAG   = "ROUTES";
let router  = express.Router();

// routes related to membersrvc needs to write. Remove get root cert; replace with enrol and get user

let membersrvc = Observable.create( observer => {
  caConnector.getECACertificate((err, cert) => {
    if (!err) {
      // console.log(TAG, `${cert}`);
      observer.next(cert);
    } else {
      // console.log(TAG, `${err}`);
      observer.onError(err);
    }
  });
});

// todo: the whole router requires integration tests
router.route("/api/mock").get((req, res) => {
  let data = { data: {key: "a lot data", value: "another data"}};
  // res.write(JSON.stringify(data));
  res.json(data);
  res.end();
});

// callback implementation, can be replaced by Promise
router.route("/api/ecaroot1").get((req, res) => {
  caConnector.getECACertificate((err, cert) => {
    if (err) {
      res.json(new Error(JSON.stringify(err)));
      res.end();
    } else {
      res.json(cert);
      res.end();
    }
  });
});

// rxjs implementation
router.route("/api/ecaroot2").get((req, res) => {
  let subscription = membersrvc.subscribe(
    value => res.json(value),
    error => res.json(error),
    () => res.end()
  );
});

router.route("/api/hyperledger/query").get((req, res) => {
  chaincode.execute(REST_CALLS.QUERY, (e, r) => {
    if (e) {
      res.status(400);
      res.end(JSON.stringify(e));
    } else {
      res.write(JSON.stringify(r));
      res.end();
    }
  });
});


export = router;
