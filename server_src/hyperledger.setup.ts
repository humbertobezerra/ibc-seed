import * as chalk            from "chalk";
import { ChainCode, Ibc, Error }    from "ibm-blockchain-js";

import * as config           from "./config";

let IBC             = require("ibm-blockchain-js");
let ibc: Ibc        = new IBC();
let chaincode: ChainCode  = null;
let callback              = null;      // todo: this can be removed. Or should replace by Promise. check it

const TAG                 = "IBC-SETUP";

/**
 * Helper class to initialize Blockchain network and deploy chaincode
 */
class Blockchain {
  users: any[] = null;
  peers: any[] = null;
  ca           = null;

  constructor () {
    this.peers = config.BLOCKCHAIN.network.peers;
    this.users = config.BLOCKCHAIN.network.users;
    this.ca = config.BLOCKCHAIN.network.ca;
  }

  /**
   * init function
   * @param cb
     */
  public init (cb: Function) { // todo: this callback is ungood implementation to return ibc and chaincode, must be revisited.
    callback = cb;
    console.log(TAG, chalk.green("Loading hardcoded peers"));
    ibc.network(this.peers);
    if (this.users && this.users.length > 0) {
      for (let user of this.users) {
        if (config.BLOCKCHAIN.network.peers[0] && user.secret && user.username) {
          ibc.register(0, user.username, user.secret, 3);
        }
      }
    }
    ibc.load_chaincode(config.BLOCKCHAIN.chaincode, cb_ready);
  }

  /**
   * implement custom READ function
   * @param args
   * @param exec
     */
  public read (args, exec) {
    chaincode.query.read(args, config.ENROLL_ID, exec);
  }

  /**
   * placeholder for custom function implementation
   * @param args
   * @param exec
     */
  public invokeFunc1 (args, exec) {
    chaincode.invoke.Func1(args, config.ENROLL_ID, exec);
  }
}

function cb_ready (err: Error, cc: ChainCode) {
  if (err !== null) {
    console.log( chalk.red(`Chaincode loading error: ${err.name}`));
    // if (!process.error) process.error = {type: 'load', msg: err.details};
  } else {
    // chaincode = cc;
    console.log(TAG, chalk.green(`Chaincode loaded: ${JSON.stringify(cc)}`));
    if (!cc.details.deployed_name || cc.details.deployed_name === "") {
      cc.deploy(config.CC_INIT, config.CC_INIT_PARAMS, config.CC_SUMMARY, cb_deployed);
    } else {
      console.log(TAG, "chaincode summary file indicates chaincode has been previously deployed");
    }
  }
}

function cb_deployed (err: Error, cc: ChainCode) {
  if (err !== null) {
    console.log(TAG, chalk.red(`CC init: ${JSON.stringify(err)}`));
    // if (!process.error) process.error = {type: "deploy", msg: err.details};
  } else {
    console.log(TAG, chalk.green(`CC init: ${JSON.stringify(cc)}`));
    // this.chaincodeID = cc.message;
    ibc.save(config.CC_SUMMARY);
    callback(ibc, cc);
  }
}

export let blockchain = new Blockchain();

// function loadVCAP () {
//   if (process.env.VCAP_SERVICES) {
//     let servicesObject = JSON.parse(process.env.VCAP_SERVICES);
//     for (let obj of servicesObject) {
//       if (obj.indexOf("ibm-blockchain") >= 0) {
//         if (obj[0].credentials.error) {
//           console.log(chalk.red(`Error from Bluemix: ${obj[0].credentials.error}`));
//           // process.error = {type: "network", msg: "Bluemix network service error"};
//         }
//       }
//       if (obj[0].credentials && obj[0].credentials.peers) {
//         console.log(chalk.green(`overwriting peers; loading from vcap service`));
//         this.peers = obj[0].credentials.peers;
//         let ca_name = Object.keys(obj[0].credentials.ca)[0];
//         console.log(chalk.green(`loading ca: ${ca_name}`));
//         this.ca = obj[0].credentials.ca[ca_name];
//         if (obj[0].credentials.users) {
//           console.log(chalk.green(`overwritting users; loading from vcap service`));
//           this.users = obj[0].credentials.users;
//         }
//       }
//     }
//   }
// }

