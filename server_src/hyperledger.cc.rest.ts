/**
 * Created by tangross on 25/4/2016.
 */
import * as request                 from "request";

import { CoreOptions, UrlOptions }  from "request";

const TAG              = "CHAINCODE";
const USER             = "jim";
const CCID             = "mycc"; // this needs to fix

export const REST_CALLS = {
  // unit test for this example
  // https://github.com/hyperledger/fabric/blob/master/docs/API/SandboxSetup.md
  "LOGIN": {
    "path": "http://localhost:3000/registrar",
    "method": "post",
    "enrollId": "jim",
    "enrollSecret": "NPKYL39uKbkj"
  },
  "DEPLOY_CC": {
    "func": "init",
    "path": "http://localhost:3000/chaincode",
    "method": "post",
    "ccMethod": "deploy",
    "args": ["a", "100", "b", "200"],
    "id": 1
  },
  "QUERY": {
    "func": "query",
    "path": "http://localhost:3000/chaincode",
    "method": "post",
    "ccMethod": "query",
    "args": ["a"],
    "id": 5
  },
  "INVOKE": {
    "func": "invoke",
    "path": "http://localhost:3000/chaincode",
    "method": "post",
    "ccMethod": "invoke",
    "args": ["a", "b", "1"],
    "id": 3
  }
};

interface BaseOption {
  path?: string;
  method?: string;
  ccMethod?: string;
  func?: string;
  args?: string[];
  id?: number;
}

/**
 * Helper class to invoke chaincode REST call
 */
class ChainCode {

  /**
   * login to peer-node
   * @param opt
   * @param callback
     */
  public login (opt: any, callback: Function) {
    let options: (UrlOptions & CoreOptions) = getLoginOptions( opt.path, opt.method, opt.enrollId, opt.enrollSecret );
    request(options, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        console.log(TAG, `Login chaincode network: ${JSON.stringify(body)}`);
        callback(null, body);
      } else {
        console.log(TAG, `Fail chaincode login: ${JSON.stringify(error)}`);
        callback(error, null);
      }
    });
  }

  /**
   * execute ChainCode function
   * @param opt
   * @param callback
     */
  public execute (opt: BaseOption, callback: Function) {
    let options: (UrlOptions & CoreOptions) = getOptions( opt.path, opt.method, opt.ccMethod, CCID, opt.func, opt.args, opt.id );
    request(options, (error, response, body) => {
      if (!error && response.statusCode === 200) {
        console.log(TAG, `Return chaincode response: ${JSON.stringify(body)}`);
        if (body.error) {
          console.error(TAG, `Error chaincode status: ${JSON.stringify(body.error)}`);
          return callback(body.error, null);
        } else {
          console.log(TAG, `Chaincode result: ${JSON.stringify(body.result)}`);
          return callback(null, body.result);
        }
      } else {
        console.error(TAG, `Fail to return chaincode response: ${JSON.stringify(error)}`);
        return callback(error, null);
      }
    });
  }
}

function getLoginOptions (...opt): (CoreOptions & UrlOptions) {
  return {
    "url": opt[0],
    "method": opt[1],
    "body": {
      "enrollId": opt[2],
      "enrollSecret": opt[3]
    },
    "json": true
  };
}

function getOptions (...opt): (CoreOptions & UrlOptions) {
  return {
    "url": opt[0],
    "method": opt[1],
    "body": {
              "jsonrpc": "2.0",
              "method": opt[2],
              "params": {
                "type": 1,
                "chaincodeID": {
                  "name": opt[3]
                },
                "ctorMsg": {
                  "function": opt[4],
                  "args": opt[5]
                }
              },
              "id": opt[6]
            },
    "json": true
  };
}


export let chaincode = new ChainCode();
