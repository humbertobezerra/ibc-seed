/**
 * Created by tangross on 14/5/2016.
 */
import { chaincode, REST_CALLS } from "../hyperledger.cc.rest";

// login as jim
describe("Chaincode RESTful endpoint", () => {
  let [err, res] = [null, null];

  beforeEach((done) => {
    chaincode.login(REST_CALLS.LOGIN, (e, r) => {
      [err, res] = [e, r];
      done();
    });
  });

  it("should perform LOGIN", () => {
    expect(res.OK).not.toBe(null);
  });
});

// deploy chaincode
describe("Chaincode RESTful endpoint", () => {
  let [err, res] = [null, null];

  beforeEach((done) => {
    chaincode.execute(REST_CALLS.DEPLOY_CC, (e, r) => {
      [err, res] = [e, r];
      done();
    });
  });

  it("should perform DEPLOY CHAINCODE", () => {
    expect(res.status).toEqual("OK");
  });
});

// invoke custom function, deducted a by 1
describe("Chaincode RESTful endpoint", () => {
  let [err, res] = [null, null];

  beforeEach((done) => {
    chaincode.execute(REST_CALLS.INVOKE, (e, r) => {
      [err, res] = [e, r];
      done();
    });
  });

  it("should perform INVOKE (and get non-empty transaction id)", () => {
    expect(res.status).toEqual("OK");
    expect(res.message.length).toBeGreaterThan(0);
  });
});

// query
describe("Chaincode RESTful endpoint", () => {
  let [err, res] = [null, null];

  beforeEach((done) => {
    chaincode.execute(REST_CALLS.QUERY, (e, r) => {
      [err, res] = [e, r];
      done();
    });
  });

  it("should perform READ (to prove invoke works well)", () => {
    expect(res.status).toEqual("OK");
  });
});
