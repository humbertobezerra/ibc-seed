/**
 * Created by tangross on 21/5/2016.
 */
import { FORM_DIRECTIVES }  from "@angular/common";
import { Component }        from "@angular/core";

import { HyperledgerService }  from "../../shared/index";

@Component({
  selector: "member",
  directives: [FORM_DIRECTIVES],
  providers: [ HyperledgerService ],
  template: `
    <div>
      <ng-content></ng-content>
      <p>ECA root certificate: {{ cert }} </p>
    </div>
  `
})

export class MembersrvcComponent  {
  cert: string;
  err: string;

  constructor(private hlService: HyperledgerService) {
    this.getECARootCertificate();
  }

  getECARootCertificate () {
    this.hlService.getECARootCertificate()
      .subscribe(
        cert  => this.cert  = <any>cert,
        error => this.err   = <any>error
      );
  }
}
